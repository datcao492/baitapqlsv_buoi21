function ValidatorSV() {
  this.kiemTraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();

    if (/*valueTarget == "" or*/ !valueTarget) {
      document.getElementById(idError).innerHTML = messageError;

      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  };

  this.kiemTraIDHopLe = function (newSinhVien, danhSachSinhVien) {
    var index = danhSachSinhVien.findIndex(function (item) {
      return item.maSV == newSinhVien.maSV;
    });

    if (index == -1) {
      document.getElementById("spanMaSV").innerText = "";
      return true;
    }
    document.getElementById("spanMaSV").innerText =
      "Mã sinh viên không được trùng";
    return false;
  };

  this.kiemTraEmail = function (idTarget, idError) {
    let parten = /^[a-z0-9](.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Email ko hợp lệ";
    return false;
  };
  this.kiemtraNumbers = function (idcheck, iderror) {
    let score = document.getElementById(idcheck).value;
    let numbers = /^[0-9]+$/;
    if (score.match(numbers) && score >= 0 && score <= 10) {
      document.getElementById(iderror).innerText = "";
      return true;
    } 
      document.getElementById(iderror).innerText = "Điểm không hợp lệ";
      return false;
  };
  this.kiemtraTenSV = function (idcheck, iderror) {
    let tenSV = document.getElementById(idcheck).value;
    let letters =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (letters.test(tenSV)) {
      document.getElementById(iderror).innerText = "";
      return true;
    } else {
      document.getElementById(iderror).innerText = "Tên sinh viên không hợp lệ";
      return false;
    }
  };
}
