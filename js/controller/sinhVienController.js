function layThongTinTuForm() {
    var maSV = document.getElementById("txtMaSV").value;
    var tenSV = document.getElementById("txtTenSV").value;
    var emailSV = document.getElementById("txtEmail").value;
    var toan = document.getElementById("txtDiemToan").value * 1;
    var ly = document.getElementById("txtDiemLy").value * 1;
    var hoa = document.getElementById("txtDiemHoa").value * 1;

    


    var sinhVien = new SinhVien(maSV, tenSV, emailSV, toan, ly, hoa)
    // console.log(sinhVien);
    return sinhVien;
}

function xuatDanhSachSV(dssv) {
    var contentHTML = "";

    for (var index = 0; index < dssv.length; index++) {
        var sinhVien = dssv[index];
        var contentTrTag = /*html*/ `<tr>
        <td>${sinhVien.maSV}</td>
        <td>${sinhVien.tenSV}</td>
        <td>${sinhVien.emailSV}</td>
        <td>${sinhVien.tinhDiemTrungBinh()}</td>

        <td>
        <button class="btn btn-success" onclick= "suaSinhVien('${sinhVien.maSV}')">Sửa</button>
        <button class="btn btn-danger" onclick= "xoaSinhVien('${sinhVien.maSV}')">Xóa</button>
        </td>
        </tr>`;

        contentHTML += contentTrTag;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xuatThongTinTrenForm(sv) {
    document.getElementById("txtMaSV").value = sv.maSV;
    document.getElementById("txtTenSV").value = sv.tenSV;
    document.getElementById("txtEmail").value = sv.emailSV;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

