var danhSachSinhVien = [];
var varlidatorSV = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSV == id;
  });
};

//lay du lieu tu localstorage khi user tai lai trang

const luuLocalStorage = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};
// lay dữ liệu từ localstorage khi user tai lai trang
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
// gắn ngược lại cho array gốc và render lại giao diện
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);

  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSV,
      item.tenSV,
      item.emailSV,
      item.diemToan,
      item.diemHoa,
      item.diemLy
    );
  });
  xuatDanhSachSV(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();

  var isValid = true;
  var index = danhSachSinhVien.findIndex(function (item) {
    return item.maSV == newSinhVien.maSV;
  });

  var isValidMaSv =
    varlidatorSV.kiemTraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã sinh viên không được để trống"
    ) && varlidatorSV.kiemTraIDHopLe(newSinhVien, danhSachSinhVien);

  var isValidEmail =
    varlidatorSV.kiemTraRong(
      "txtEmail",
      "spanEmailSV",
      "Email sinh viên không được để trống"
    ) && varlidatorSV.kiemTraEmail("txtEmail", "spanEmailSV");

  var isValidTenSV =
    varlidatorSV.kiemTraRong(
      "txtTenSV",
      "spanTenSV",
      "Tên sinh viên không được để trống"
    ) && varlidatorSV.kiemtraTenSV("txtTenSV", "spanTenSV");

  var isValidDiemSV =
    varlidatorSV.kiemTraRong(
      "txtDiemToan",
      "spanToan",
      "Điểm sinh viên không được để trống"
    ) && varlidatorSV.kiemtraNumbers("txtDiemToan", "spanToan");

  var isValidDiemSV =
    varlidatorSV.kiemTraRong(
      "txtDiemLy",
      "spanLy",
      "Điểm sinh viên không được để trống"
    ) && varlidatorSV.kiemtraNumbers("txtDiemLy", "spanLy");

  var isValidDiemSV =
    varlidatorSV.kiemTraRong(
      "txtDiemHoa",
      "spanHoa",
      "Điểm sinh viên không được để trống"
    ) && varlidatorSV.kiemtraNumbers("txtDiemHoa", "spanHoa");

  isValid = isValidMaSv && isValidEmail && isValidTenSV && isValidDiemSV;

  // console.log(index);
  if (isValid == true) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSV(danhSachSinhVien);
    // document.getElementById("spanMaSV").innerText = "";

    // convert array thành json để có thể lưu vào localstorage
    luuLocalStorage();
    //   console.log({danhSachSinhVien});
    // }else{
    //   document.getElementById("spanMaSV").innerText = "Mã SV không được trùng";
    // }
  }
}
function xoaSinhVien(id) {
  console.log(id);

  var viTri = timKiemViTri(id, danhSachSinhVien);
  console.log({ viTri });
  // xóa tại vị trí tìm kiếm

  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSV(danhSachSinhVien);
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);
  console.log({ viTri });

  var sinhVien = danhSachSinhVien[viTri];
  xuatThongTinTrenForm(sinhVien);
}

function capNhatSinhVien() {
  var isValid = true;
  if (isValid) {
    let sinhVienedited = layThongTinTuForm();
    let position = timKiemViTri(sinhVienedited.maSV, danhSachSinhVien);
    if (position != -1) {
      danhSachSinhVien[position] = sinhVienedited;
      xuatDanhSachSV(danhSachSinhVien);
      document.getElementById("spanMaSV").innerText = "";
    } else {
      document.getElementById("spanMaSV").innerText =
        "Không được thay đổi Mã Sinh Viên";
    }

    luuLocalStorage();
  }
}

function reset() {
  document.getElementById("txtMaSV").disabled = false;
  document.querySelector("#txtMaSV").value = "";
  document.querySelector("#txtTenSV").value = "";
  document.querySelector("#txtEmail").value = "";
  document.querySelector("#txtDiemToan").value = "";
  document.querySelector("#txtDiemLy").value = "";
  document.querySelector("#txtDiemHoa").value = "";
}

function searchSV() {
  let SearchSVArr = [];
  let search = document.getElementById("txtSearch").value;
  
  danhSachSinhVien.forEach(function (item) {
    // console.log(item.tenSV);
    if (item.tenSV == search) {
      SearchSVArr.push(item);
    }
  });
  if (SearchSVArr != "") {
    xuatDanhSachSV(SearchSVArr);
    document.getElementById("err").innerText = "";
  } else {
    xuatDanhSachSV(SearchSVArr);
    document.getElementById("err").innerText = "Không tìm thấy Sinh Viên";
  }
}

function hienThiLaiDssvKhiXoaSearch() {
  var txtSearch = document.getElementById("txtSearch").value;
  if (txtSearch == "") {
    xuatDanhSachSV(danhSachSinhVien);
  }
}